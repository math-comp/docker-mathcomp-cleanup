#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (c) 2019-2023  Érik Martin-Dorel
#
# Tool to remove old mathcomp-dev images from GitLab Docker registry.
#
# Licensed under BSD-3 <https://opensource.org/licenses/BSD-3-Clause>
#
# Report bugs to the dedicated issues tracker.

import copy
import json
import re
import requests
import sys
import time

# constants
version = "0.2.1"
api_url = "https://gitlab.inria.fr/api/v4"
regexp = re.compile(r'\A([0-9]+)_([a-z0-9-]+)_((?:coq|rocq-prover)-[a-z0-9.-]+)\Z')
mathcomp_filename = './mathcomp-dev.json'
mathcomp_project_id = 44939
mathcomp_repo_id = 2326
# constant for Step 2.1 (see find_delete_tags)
any_keep_last_iids = 1
# constant for Step 2.2 (see find_delete_tags)
master_keep_last_iids = 2


def error(msg, flush=True):
    print(msg, file=sys.stderr, flush=flush)
    exit(1)


def uniqify(s):
    """Remove duplicates, without preserving the elements order."""
    return list(set(s))


def slugify(string):
    """https://gitlab.com/gitlab-org/gitlab-ce/blob/d4ef3be35b63f3ef022e21d6ba56ffe41b8f192c/lib/gitlab/utils.rb#L48-59"""
    # # A slugified version of the string, suitable for inclusion in URLs and
    # # domain names. Rules:
    # #
    # #   * Lowercased
    # #   * Anything not matching [a-z0-9-] is replaced with a -
    # #   * Maximum length is 63 bytes
    # #   * First/Last Character is not a hyphen
    # def slugify(str)
    #   return str.downcase
    #     .gsub(/[^a-z0-9]/, '-')[0..62]
    #     .gsub(/(\A-+|-+\z)/, '')
    # end
    regex1 = r"[^a-z0-9]"
    regex2 = r"(\A-+|-+\Z)"
    return re.sub(regex2, "",
                  re.sub(regex1, "-",
                         string.lower(), 0)[0:63], 0)


def dump(data):
    """DEBUG"""
    print(json.dumps(data, indent=4), flush=True)


def build_params_pagination(page, per_page):
    """https://docs.gitlab.com/ce/api/README.html#pagination"""
    return {
        'page': str(page),
        'per_page': str(per_page)
    }


def build_params_search(search_str):
    """https://docs.gitlab.com/ee/api/branches.html#list-repository-branches"""
    return {
        'search': str(search_str)
    }


def build_headers_token(token):
    """https://docs.gitlab.com/ce/api/README.html#personal-access-tokens"""
    return {
        'PRIVATE-TOKEN': token,
    }


def merge_dict(a, b):
    res = copy.deepcopy(a) if a else {}
    copyb = copy.deepcopy(b) if b else {}
    for key in copyb:
        res[key] = copyb[key]
    return res


def get_list_paginated(url, headers, params, max_per_sec=5):
    """Generic wrapper to handle GET requests with pagination"""
    assert isinstance(max_per_sec, int)
    assert max_per_sec > 0
    assert max_per_sec <= 10
    per_page = 100  # the maximum allowed by gitlab.com
    page = 0
    allj = []
    while(True):
        page += 1
        if page % max_per_sec == 0:
            time.sleep(1.1)
        page_params = build_params_pagination(page, per_page)
        all_params = merge_dict(params, page_params)
        print("GET %s\n  # page: %d"
              % (url, page), file=sys.stderr, flush=True)
        response = requests.get(url, headers=headers, params=all_params)
        if not response:
            error("Error!\nCode: %d\nText: %s"
                  % (response.status_code, response.text))
        j = response.json()
        if not isinstance(j, list):
            error("Error: not JSON list\nText: %s"
                  % response.text)
        if j:
            allj += j
        else:
            break
    return allj


def get_list_git_branches(project_id, search_str=''):
    url = api_url + ("/projects/%d/repository/branches"
                     % project_id)
    headers = None
    params = build_params_search(search_str) if search_str else None
    res = get_list_paginated(url, headers, params)
    return list(map(lambda e: e["name"], res))


def get_list_git_branches_slug(project_id, search_str=''):
    return list(map(slugify, get_list_git_branches(project_id, search_str)))


def get_docker_repos(project_id, token=''):
    """Note: not used by the script itself.
    See test_get_docker_repos_mathcomp."""
    url = api_url + ("/projects/%d/registry/repositories"
                     % project_id)
    headers = build_headers_token(token) if token else None
    # params = None
    return requests.get(url, headers=headers)  # No pagination


def get_list_docker_tags(project_id, repo_id, token='', max_per_sec=5):
    url = api_url + ("/projects/%d/registry/repositories/%d/tags"
                     % (project_id, repo_id))
    headers = build_headers_token(token) if token else None
    params = None
    raw_list = get_list_paginated(url, headers, params)
    # ErikMD: commenting out this code given the migration to gitlab.inria.fr
    # # Manually exclude a buggy tag that originated on 2021-06-12T17:17:00+02:00
    # # in this CI job: https://gitlab.com/math-comp/math-comp/-/jobs/1341807695
    # buggy_tag = "registry.gitlab.com/math-comp/math-comp:3334_master_coq-dev"
    # return list(filter(lambda e: e["location"] != buggy_tag, raw_list))
    return raw_list


def write_list_docker_tags(project_id, repo_id, filename, token=''):
    json_data = get_list_docker_tags(project_id, repo_id, token)
    with open(filename, 'w') as f:
        json.dump(json_data, f, indent=4)


def read_list_docker_tags(filename):
    with open(filename) as f:
        j = json.load(f)
    return j


def parse_tag(tag):
    name = tag["name"]
    mat = re.match(regexp, name)
    if mat:
        return {'name': name,
                # we assume mat.group(1).isdecimal() holds
                'iid': int(mat.group(1)),
                'branch': mat.group(2),
                'coq': mat.group(3)}
    else:
        error("tag name '%s' doesn't match the regexp" % name)


def delete_tag(project_id, repo_id, tag_name, token='', force=False):
    """https://docs.gitlab.com/ee/api/container_registry.html#delete-a-repository-tag"""
    url = api_url + ("/projects/%d/registry/repositories/%d/tags/%s"
                     % (project_id, repo_id, tag_name))
    headers = build_headers_token(token) if token else None
    if force:
        print("DELETE %s" % url, file=sys.stderr, flush=True)
        response = requests.delete(url, headers=headers)
        if not response:
            error("Error!\nCode: %d\nText: %s"
                  % (response.status_code, response.text), flush=True)
        else:
            print("Code: %d\nText: %s"
                  % (response.status_code, response.text), flush=True)
    else:
        print("Dry-Run DELETE %s" % url, file=sys.stderr, flush=True)


def delete_tags(project_id, repo_id, tags, token='', force=False,
                max_per_sec=9):
    """Wrapper to handle DELETE requests with throttle.
    See https://docs.gitlab.com/ce/user/gitlab_com/index.html#gitlabcom-specific-rate-limits"""
    assert isinstance(max_per_sec, int)
    assert max_per_sec > 0
    assert max_per_sec <= 10
    total = len(tags)
    num = 0
    for tag in tags:
        num += 1
        print("(%d/%d) " % (num, total), end='', file=sys.stderr, flush=True)
        delete_tag(project_id, repo_id, tag['name'], token, force)
        if num % max_per_sec == 0 and force:
            time.sleep(1.1)


def find_delete_tags(project_id, repo_id, token='', force=False):
    """1. remove all docker tags that have no related branch slug
    2.1. for existing branches (except master): keep the last pipeline IID(s)
    2.2. for branch master: keep the last 2 pipeline IID(s)"""
    raw_json = read_list_docker_tags(mathcomp_filename)
    tags = list(map(parse_tag, raw_json))
    tags.sort(key=lambda t: t['iid'])
    slugs = get_list_git_branches_slug(project_id)

    print('0. Current branch slugs (number=%d):' % len(slugs), flush=True)
    print(json.dumps(slugs, indent=4), flush=True)

    # Step 1.
    assert 'master' in slugs
    noslug_tags_todel = list(filter(lambda t: t['branch'] not in slugs, tags))

    print('\n1. Removing %d tags with no related branch slug' %
          len(noslug_tags_todel), flush=True)
    # DEBUG
    # dump(noslug_tags_todel)
    delete_tags(project_id, repo_id, noslug_tags_todel, token, force)

    # Step 2.1.
    any_tags = list(filter(lambda t: t['branch'] in slugs
                           and t['branch'] != 'master', tags))
    any_keep_iids = {}  # slug => [iid1, iid2]
    for b in slugs:
        if b != 'master':
            tmp = [t['iid'] for t in any_tags if t['branch'] == b]
            any_keep_iids[b] = sorted(uniqify(tmp))[-any_keep_last_iids:]

    any_tags_todel = [t for t in any_tags
                      if t['iid'] not in any_keep_iids[t['branch']]]

    print('\n2.1. Removing %d old tags for existing branches (except master)' %
          len(any_tags_todel), flush=True)
    # DEBUG
    # dump(any_tags_todel)
    delete_tags(project_id, repo_id, any_tags_todel, token, force)

    print('We kept the following (branch => iid):', flush=True)
    dump(any_keep_iids)

    # Step 2.2
    master_tags = list(filter(lambda t: t['branch'] == 'master', tags))
    # if not master_tags:
    #     error('Error: master_tags should be nonempty.')
    tmp = [t['iid'] for t in master_tags]
    master_keep_iids = sorted(uniqify(tmp))[-master_keep_last_iids:]

    master_tags_todel = [t for t in master_tags
                         if t['iid'] not in master_keep_iids]

    print('\n2.2. Removing %d old tags for branch master' %
          len(master_tags_todel), flush=True)
    # DEBUG
    # dump(master_tags_todel)
    delete_tags(project_id, repo_id, master_tags_todel, token, force)

    print('We kept the following master IIDs:', flush=True)
    dump(master_keep_iids)


def usage():
    print("""# docker-mathcomp-cleanup

This python script is meant to be run automatically by GitLab CI.

Still, it can be used directly from command-line.

## Syntax

```
cleanup.py write-tags
    Dump the list of Docker tags to file %s

cleanup.py delete-tags -n
    Delete tags w.r.t. the mathcomp-dev removal policy (dry-run)

cleanup.py delete-tags -f TOKEN
    Delete tags w.r.t. the mathcomp-dev removal policy (force!)

cleanup.py --version
    Print the script version

cleanup.py --help
    Print this documentation
```

## Usage

* Create a gitlab.com personal access token (say, ABC) with scope api:
    * <https://docs.gitlab.com/ce/user/profile/personal_access_tokens.html>
    * <https://gitlab.com/profile/personal_access_tokens>

* Run the following commands:

```
./cleanup.py write-tags
./cleanup.py delete-tags -n  # optional
./cleanup.py delete-tags -f ABC
```

## Author and License

This tool was written by [Érik Martin-Dorel](
https://gitlab.com/erikmd) and it is distributed under the
[BSD-3](https://opensource.org/licenses/BSD-3-Clause) license.
""" % mathcomp_filename)


def main(argv):
    if argv == ['--version']:
        print('cleanup.py version %s' % version, flush=True)
    elif argv == ['write-tags']:
        write_list_docker_tags(mathcomp_project_id, mathcomp_repo_id,
                               './mathcomp-dev.json')
        print('done.')
    elif argv == ['delete-tags', '-n']:
        find_delete_tags(mathcomp_project_id, mathcomp_repo_id,
                         token='', force=False)
    elif argv[0:2] == ['delete-tags', '-f'] and len(argv) == 3:
        find_delete_tags(mathcomp_project_id, mathcomp_repo_id,
                         token=argv[2], force=True)
        print('done.')
    else:
        usage()


# Note: the issue https://gitlab.com/gitlab-org/gitlab-ce/issues/21405
# is now resolved by this MR:
# https://gitlab.com/gitlab-org/gitlab/-/merge_requests/16886

###############################################################################
# Test suite, cf. <https://docs.python-guide.org/writing/tests/>
# $ pip3 install pytest
# $ py.test cleanup.py


def test_slugify():
    """https://gitlab.com/gitlab-org/gitlab-ce/blob/d4ef3be35b63f3ef022e21d6ba56ffe41b8f192c/spec/lib/gitlab/utils_spec.rb#L33-45"""
    assert slugify('TEST') == 'test'
    assert slugify('project_with_underscores') == 'project-with-underscores'
    assert slugify('namespace/project') == 'namespace-project'
    assert slugify('a' * 70) == 'a' * 63
    assert slugify('test_trailing_') == 'test-trailing'


def test_get_list_git_branches_slug_mathcomp():
    """id[https://gitlab.com/math-comp/math-comp] = mathcomp_project_id"""
    resA = get_list_git_branches_slug(mathcomp_project_id)
    resPR = get_list_git_branches_slug(mathcomp_project_id, 'pr')
    print(json.dumps(resPR))
    print(json.dumps(resA))
    assert len(resPR) < len(resA)


def test_get_docker_repos_mathcomp():
    """id[https://gitlab.com/math-comp/math-comp] = mathcomp_project_id"""
    response = get_docker_repos(mathcomp_project_id)
    assert response
    res = response.json()
    print(json.dumps(res))
    assert res[0]["id"] == mathcomp_repo_id


if __name__ == "__main__":
    main(sys.argv[1:])
