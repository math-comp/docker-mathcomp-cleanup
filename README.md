# docker-mathcomp-cleanup

This python script is meant to be run automatically by GitLab CI.

Still, it can be used directly from command-line.

## Syntax

```
cleanup.py write-tags
    Dump the list of Docker tags to file ./mathcomp-dev.json

cleanup.py delete-tags -n
    Delete tags w.r.t. the mathcomp-dev removal policy (dry-run)

cleanup.py delete-tags -f TOKEN
    Delete tags w.r.t. the mathcomp-dev removal policy (force!)

cleanup.py --version
    Print the script version

cleanup.py --help
    Print this documentation
```

## Usage

* Create a gitlab.com personal access token (say, ABC) with scope api:
    * <https://docs.gitlab.com/ce/user/profile/personal_access_tokens.html>
    * <https://gitlab.com/profile/personal_access_tokens>

* Run the following commands:

```
./cleanup.py write-tags
./cleanup.py delete-tags -n  # optional
./cleanup.py delete-tags -f ABC
```

## Author and License

This tool was written by [Érik Martin-Dorel](
https://gitlab.com/erikmd) and it is distributed under the
[BSD-3](https://opensource.org/licenses/BSD-3-Clause) license.

