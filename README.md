# docker-mathcomp-cleanup

This python script is meant to be run automatically by GitLab CI.

Still, it can be used directly from command-line.

It specifically targets [math-comp's Container Registry](https://gitlab.inria.fr/math-comp/math-comp/container_registry), populated with the Docker images from [math-comp's Pipelines for master](https://gitlab.inria.fr/math-comp/math-comp/-/pipelines?page=1&scope=all&ref=master).

## Syntax

```
cleanup.py write-tags
    Dump the list of Docker tags to file ./mathcomp-dev.json

cleanup.py delete-tags -n
    Delete tags w.r.t. the mathcomp-dev removal policy (dry-run)

cleanup.py delete-tags -f TOKEN
    Delete tags w.r.t. the mathcomp-dev removal policy (force!)

cleanup.py --version
    Print the script version

cleanup.py --help
    Print this documentation
```

## Usage

* Fetch `mathcomp_project_id` from <https://gitlab.inria.fr/math-comp/math-comp> → 44939

* Fetch `mathcomp_repo_id` using `curl https://gitlab.inria.fr/api/v4/projects/44939/registry/repositories` → 2326

* Update [cleanup.py](./cleanup.py)'s constants accordingly.

* Create a gitlab.com personal access token (say, `$PAC`) with scope `api`:
    * <https://docs.gitlab.com/ce/user/profile/personal_access_tokens.html>
    * <https://gitlab.inria.fr/-/profile/personal_access_tokens>

* Run the following commands:

  ```
  ./cleanup.py write-tags
  cat mathcomp-dev.json
  ./cleanup.py delete-tags -n  # optional
  ./cleanup.py delete-tags -f "$PAC"
  ```

* Open [General Settings](https://gitlab.inria.fr/math-comp/docker-mathcomp-cleanup/edit) to Enable CI/CD etc.

* Open [CI/CD Settings](https://gitlab.inria.fr/math-comp/docker-mathcomp-cleanup/-/settings/ci_cd) to Enable Shared runners, Add variable `CLEANUP_TOKEN` (:= `$PAC`, protected, masked, in env. cleanup)

* Open [CI/CD Schedules](https://gitlab.inria.fr/math-comp/docker-mathcomp-cleanup/-/pipeline_schedules) to Add Pipeline Schedule `daily-cleanup` at [`0 6 * * *`](https://crontab.guru/#0_6_*_*_*) or so [(doc)](https://docs.gitlab.com/ee/topics/cron/), Add variable `CRON_MODE` (:= `true-clean`)

## Author and License

This tool was written by [Érik Martin-Dorel](
https://gitlab.com/erikmd) and it is distributed under the
[BSD-3](https://opensource.org/licenses/BSD-3-Clause) license.

